package com.examptn.esprit.arcticle.dereservationservicemo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DereservationServicemoApplication {

	public static void main(String[] args) {
		SpringApplication.run(DereservationServicemoApplication.class, args);
	}

}
